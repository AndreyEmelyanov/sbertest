package task2;


public interface Figure
{
    /**Площадь фигуры*/
    public double getSquare();
    /**Периметр фигуры*/
    public double getPerimeter();
}
