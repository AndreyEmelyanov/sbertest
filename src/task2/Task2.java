package task2;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Task2
{
    public static void main(String[] args)
    {
        Map<String, Figure> map = new HashMap<String, Figure>();
        Random random = new Random();
        Figure figure;
        int max = random.nextInt(50)+1;
        for (int i = 0; i < max; i++)
        {
            int key = random.nextInt(3);
            switch (key)
            {
                case 0:
                    figure = new Circle(random.nextInt(50)+1);
                    map.put(i + " Круг", figure);
                    break;
                case 1:
                    figure = new Rectangle(random.nextInt(50)+1, random.nextInt(50)+1);
                    map.put(i + " Прямоугольник", figure);
                    break;
                case 2:
                    figure = new ReuleauxTriangle(random.nextInt(50)+1);
                    map.put(i + " Треугольник Релё", figure);
                    break;
            }
        }
        for (Map.Entry entry : map.entrySet())
        {
            figure = (Figure) entry.getValue();
            System.out.println(entry.getKey() + " \nПлощадь: " + figure.getSquare() + "\nПериметр: " + figure.getPerimeter());
        }
    }
}
