package task2;

/**https://ru.wikipedia.org/wiki/Треугольник_Рёло*/
public class ReuleauxTriangle implements Figure
{
    /**
     * Ширина треугольника Рёло.
     * <p>
     *Подробнее https://ru.wikipedia.org/wiki/Треугольник_Рёло
     */
    private double width;

    public ReuleauxTriangle()
    {
    }

    public ReuleauxTriangle(double width)
    {
        this.width = width;
    }

    public double getWidth()
    {

        return width;
    }

    public void setWidth(double width)
    {
        this.width = width;
    }

    @Override
    public double getSquare()
    {
        return ((Math.PI-Math.sqrt(3))*width*width)/2;
    }

    @Override
    public double getPerimeter()
    {
        return Math.PI*width;
    }
}
