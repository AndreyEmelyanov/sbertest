package task2;


public class Rectangle implements Figure
{
    /**Ширина прямоугольника*/
    private double a;
    /**Высота прямоугольника*/
    private double b;

    public Rectangle()
    {
    }

    public Rectangle(double a, double b)
    {
        this.a = a;
        this.b = b;
    }

    public double getB()
    {

        return b;
    }

    public void setB(double b)
    {
        this.b = b;
    }

    public double getA()
    {
        return a;
    }

    public void setA(double a)
    {
        this.a = a;
    }

    @Override
    public double getSquare()
    {
        return a*b;
    }

    @Override
    public double getPerimeter()
    {
        return (a+b)*2;
    }
}
