package task1;


import java.util.ArrayDeque;
import java.util.Deque;

public class PolishNotation
{
    public static String inputToPolish(String input)
    {
        double doubleEl = 0;
        double v = 0;
        char[] chInput = input.toCharArray();
        StringBuilder output = new StringBuilder();
        Deque<Character> queue = new ArrayDeque<Character>();
        for (char el : chInput)
        {
            if (Character.isDigit(el))//если число, то добавляем выходную строку
            {
                doubleEl = Character.digit(el, 10);
                output.append(el);
            }
            else
            {
                if (el == '(')//открывающаяся скобка (знак с приоритетом 0) помещается в стек
                    queue.add('(');
                else
                {
                    if (queue.isEmpty() || getPriority(el) > getPriority(queue.peekLast()))//если приоритет знака операции больше приоритета знака на вершине стека или стек пуст, новый знак добавляется в стек
                        queue.add(el);
                    else
                        if ((el != ')') && (getPriority(el) <= getPriority(queue.peekLast())))//если приоритет знака меньше или равен приоритету знака на вершине стека, из стека в выходную строку “выталкиваются” все знаки с приоритетами, большими или равными приоритету входного знака. После этого входной знак записывается в стек
                        {
                            while (getPriority(queue.peekLast()) <= getPriority(el))
                            {
                                output.append(queue.pollLast());
                            }
                            output.append(queue.pollLast());
                            queue.add(el);
                        }
                        else
                            if (el == ')')//закрывающаяся скобка выталкивается в выходную строку все знаки до открывающейся скобки. Открывающаяся скобка удаляется из стека, а закрывающаяся туда не записывается.
                            {
                                while (queue.peekLast() != '(')
                                {
                                    output.append(queue.pollLast());
                                }
                                queue.pollLast();
                            }
                }
            }
        }
        while (!queue.isEmpty())//после просмотра всех символов входной строки из стека выталкиваются оставшиеся знаки
        {
            output.append(queue.pollLast());
        }
        return output.toString();
    }

    public static int getPriority(char operation)
    {
        switch (operation)
        {
            case '*':
                return 3;
            case '/':
                return 3;
            case '+':
                return 2;
            case '-':
                return 2;
            case '(':
                return 0;
            case ')':
                return 1;
        }
        return -1;
    }

    public static double polishToResult(String input)
    {
        double doubleEl;
        double v = 0;
        char[] chInput = input.toCharArray();
        Deque<Double> queue = new ArrayDeque<Double>();
        for (char el : chInput)
        {
            if (Character.isDigit(el))//если число, то добавляется в стек
            {
                doubleEl = Character.digit(el, 10);
                queue.add(doubleEl);
            }
            else// иначе выполняется операция с двумя последними элементами стека
            {
                switch (el)
                {
                    case '+':
                        v = queue.pollLast() + queue.pollLast();
                        queue.add(v);
                        break;
                    case '-'://в конце стека правый операнд
                        v = queue.pollLast();
                        v = queue.pollLast() - v;
                        queue.add(v);
                        break;
                    case '*':
                        v = queue.pollLast() * queue.pollLast();
                        queue.add(v);
                        break;
                    case '/'://в конце стека правый операнд
                        v = queue.pollLast();
                        if (v!=0)//деление на 0
                        {
                            v = queue.pollLast() / v;
                            queue.add(v);
                        }
                        else
                            return Double.NaN;
                        break;
                }
            }
        }
        return queue.pollLast();
    }
}
