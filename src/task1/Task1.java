package task1;


import java.util.Scanner;

public class Task1
{
    public static void main(String[] args)
    {
        /*  Ввод формулы(String)
            Ввод значений параметров
            Для каждого параметра: формула.replace(параметр, значение параметра);
            вызовы функций inputToPolish,polishToResult
            вывод результатов
         */
        System.out.println("Введите строку");
        Scanner in = new Scanner(System.in);
        String input = in.next();
        input = PolishNotation.inputToPolish(input);
        Double result = PolishNotation.polishToResult(input);
        if (result != Double.NaN)
            System.out.println(result);
        else
            System.out.println("Деление на 0");
    }
}
