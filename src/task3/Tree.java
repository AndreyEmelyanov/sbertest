package task3;


import java.util.ArrayList;

public class Tree
{
    private Node node;


    public Tree(Node node)
    {
        this.node = node;
    }

    public Node getNode()
    {
        return node;
    }

    public void setNode(Node node)
    {
        this.node = node;
    }

    @Override
    public int hashCode()
    {
        return node.hashCode();
    }

    private class Node
    {
        private ArrayList<Node> parents;
        private ArrayList<Node> childs;

        private boolean root;

        public Node(ArrayList<Node> parents, ArrayList<Node> childs)
        {
            this.parents = parents;
            this.childs = childs;
        }

        public ArrayList<Node> getParents()
        {
            return parents;
        }

        public void setParents(ArrayList<Node> parents)
        {
            this.parents = parents;
        }

        public ArrayList<Node> getChilds()
        {
            return childs;
        }

        public void setChilds(ArrayList<Node> childs)
        {
            this.childs = childs;
        }

        public void setRoot(boolean root)
        {
            this.root = root;
        }

        public boolean isRoot()
        {
            return root;
        }

        @Override
        public int hashCode()
        {
            int result=42;
            if (childs.size()!=0)
                result*=childs.size();
            if (parents.size()!=0)
                result*=parents.size();
            for(Node el:childs)
                result+=el.hashCode();
            return result;
        }
    }
}

