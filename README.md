# README #

SberTech test task

1.	Решить задачу на языке программирования Java :
Есть формула с цифрами, операциями +-*/ и скобками ( ). Нужно написать
программу, которая ее вычисляет.  Формула может быть большой. 
Дополнительная часть: предусмотреть (архитектурно) возможность доработки –
возможность вводить функции, вводить параметры для расчета формулы.

2.	Реализовать объектную модель на любом языке программирования:
Объекты - геометрические фигуры (любые три).  Нужно описать их с точки зрения
расчета площади и периметра.
Также необходимо предусмотреть  и написать пример их использования в коллекциях, в
частности, в ассоциативных массивах.

3.	 Имеется дерево, каждый элемент которого является объектом типа Node со
ссылками на родительские и дочерние ноды, а так же признак «isRoot» для
корневой ноды. Необходимо написать логику сравнения произвольных ветвей, а
так же реализовать хэш функцию.
